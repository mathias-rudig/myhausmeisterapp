package com.company;

import exceptions.*;
import buildingstructure.Raum;
import buildingstructure.RaumListe;
import buildingstructure.Stockwerk;
import buildingstructure.StockwerkListe;
import interference.Gewerke;
import interference.SammlungStoerungen;
import interference.StatusStoerung;
import interference.Stoerung;

/**
 * Klasse die Dummy-Datensätze anlegt.
 * @author mathiasrudig
 * @version 1.0
 */
public class DummyDateien {
    public static void dummyDateienGenerieren(SammlungStoerungen sammlungStoerungen, StockwerkListe stockwerkListe, RaumListe raumliste) {
        //Stockwerke anlegen
        Stockwerk erstesObergeschoss = null;
        Stockwerk zweitesObergeschoss = null;
        Stockwerk erdgeschoss = null;
        try {
            erstesObergeschoss = new Stockwerk( "1. OG");
            zweitesObergeschoss = new Stockwerk("2. OG");
            erdgeschoss = new Stockwerk("EG");
        } catch (InvalidInputException invalidInputException) {
            System.out.println(invalidInputException.getMessage());
        }

        //Räume anlegen
        Raum zimmer001 = null;
        Raum zimmer002 = null;
        Raum lager101 = null;
        Raum technikraum201 = null;
        try {
            zimmer001 = new Raum("001", "Zimmer", erdgeschoss);
            zimmer002 = new Raum("002", "Zimmer", erdgeschoss);
            lager101 = new Raum("101", "Lager", erstesObergeschoss);
            technikraum201 = new Raum("201", "Technikraum", zweitesObergeschoss);
        } catch (InvalidFloorException | InvalidInputException exception) {
            System.out.println(exception.getMessage());
        }

        //Stockwerke der Liste hinzufügen
        try {
            stockwerkListe.hinzufuegen(erstesObergeschoss);
            stockwerkListe.hinzufuegen(zweitesObergeschoss);
            stockwerkListe.hinzufuegen(erdgeschoss);
        } catch (InvalidFloorException invalidFloorException) {
            System.out.println(invalidFloorException.getMessage());
        }

        //Räume der Liste hinzufügen
        try {
            raumliste.hinzufuegen(zimmer001);
            raumliste.hinzufuegen(zimmer002);
            raumliste.hinzufuegen(lager101);
            raumliste.hinzufuegen(technikraum201);
        } catch (InvalidRoomException invalidRoomException) {
            System.out.println(invalidRoomException.getMessage());
        }


        //Störungen anlegen
        Stoerung stoerungEins = null;
        Stoerung stoerungZwei = null;
        Stoerung stoerungDrei = null;
        Stoerung stoerungVier = null;
        try {
            stoerungEins = new Stoerung("Tür klemmt", "Badezimmertür", Gewerke.ALLGEMEIN, zimmer001, StatusStoerung.OFFEN);
            stoerungZwei = new Stoerung("WC verstopft", "Badezimmer", Gewerke.HLK, zimmer002, StatusStoerung.ERLEDIGT);
            stoerungDrei = new Stoerung("Brandmelder defekt", "Austausch", Gewerke.ELEKTRO, lager101, StatusStoerung.OFFEN);
            stoerungVier = new Stoerung("Sicherungsausfall", "SicherungF12", Gewerke.ELEKTRO, technikraum201, StatusStoerung.IN_ARBEIT);
        } catch (InvalidInputException | InvalidStatusException | InvalidSubsectionsException | InvalidRoomException exception) {
            System.out.println(exception.getMessage());
        }
        //Sörungen der Liste hinzufügen
        try {
            sammlungStoerungen.stoerungHinzufuegen(stoerungEins);
            sammlungStoerungen.stoerungHinzufuegen(stoerungZwei);
            sammlungStoerungen.stoerungHinzufuegen(stoerungDrei);
            sammlungStoerungen.stoerungHinzufuegen(stoerungVier);
        } catch (InvalidStoerungException invalidStoerungException) {
            invalidStoerungException.printStackTrace();
        }
    }
}
