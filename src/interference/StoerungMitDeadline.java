package interference;

import exceptions.*;
import buildingstructure.Raum;
import utility.DateBuilder;

import java.io.Serializable;
import java.util.GregorianCalendar;

/**
 * Die Klasse implementiert eine Störung mit Deadline und implementiert das Marker-Interface Serializable, um die
 * erzeugten Objekte in eine Binärdatei speichern respektive laden zu können. Zudem erbt die Klasse alle
 * Eigenschaften der Mutterklasse Störung.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class StoerungMitDeadline extends Stoerung implements Serializable {
    private GregorianCalendar deadlineDatum;

    /**
     * Der Konstruktor ruft beim jeweiligen setzen der Datenfelder immer die jeweilige Setter-Methode auf, um
     * die übergebenen Werte prüfen zu können. Zudem müssen die jeweiligen Exceptions(InvalidInputException,
     * InvalidSubsectionsException, InvalidRoomException, InvalidStatusException) beim Erstellen gehandelt werden.
     *
     * @param titel         übergebener Titel vom Typ String
     * @param beschreibung  übergebene Beschreibung vom Typ String
     * @param gewaerk       übergebenes Gewerk vom Typ Geweerk
     * @param raum          übergebener Raum vom Typ Raum
     * @param status        übergebener Status vom Typ StatusStoerung
     * @param deadlineDatum übergebenes Datum für eine Deadline vom Typ GregorianCalendar
     * @throws InvalidInputException       beim Übergeben einer ungültigen Eingabe Typ String.
     * @throws InvalidSubsectionsException beim Übergeben eines ungültigen Gewerkes vom Typ Gewerk.
     * @throws InvalidRoomException        beim Übergeben eines ungültigen Raumes vom Typ Raum.
     * @throws InvalidStatusException      beim Übergeben von ungültigen Status vom Typ StatusStoerung.
     * @throws InvalidDateException        beim Übergeben eines ungültigen Datums vom Typ GregorianCalendar.
     */
    public StoerungMitDeadline(String titel, String beschreibung, Gewerke gewaerk, Raum raum, StatusStoerung status, GregorianCalendar deadlineDatum) throws InvalidInputException, InvalidDateException, InvalidStatusException, InvalidSubsectionsException, InvalidRoomException {
        super(titel, beschreibung, gewaerk, raum, status);
        this.setDeadline(deadlineDatum);
    }

    /**
     * Setter Methode, welche das Datum der Deadline der Störung setzt und bei Übergabe eines ungueltigen
     * Datums(Datum nicht in Zukunft) eine Exception(InvalidDateException) auslöst.
     *
     * @param deadlineDatum übergebenes Datum der Deadline vom Typ GregorianCalendar
     * @throws InvalidDateException beim Übergeben eines ungültigen Datums vom Typ GregorianCalendar.
     */
    public void setDeadline(GregorianCalendar deadlineDatum) throws InvalidDateException {
        if (deadlineDatum != null && deadlineDatum.after(DateBuilder.getAktuellesDatum())) {
            this.deadlineDatum = deadlineDatum;
        } else {
            throw new InvalidDateException();
        }
    }

    /**
     * Getter-Methode, welche das Datum der Deadline zurückgibt.
     *
     * @return gibt das Datum der Deadline vom Typ GregorianCalendar zurück
     */
    public GregorianCalendar getDeadline() {
        return this.deadlineDatum;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("%" + Stoerung.ANZAHL_ZEICHEN_DATUM_FORMAT_LONG + "s", DATUM_FORMAT_LONG.format(this.deadlineDatum.getTime()));
    }
}
