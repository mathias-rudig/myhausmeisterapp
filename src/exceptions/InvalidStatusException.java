package exceptions;
/**
 * @author mathiasrudig
 * @version 1.0
 */
public class InvalidStatusException extends Exception {
    public InvalidStatusException(String message) {
        super(message);
    }
    public InvalidStatusException() {
        super("Ungueltiger Status");
    }
}
