package exceptions;
/**
 * @author mathiasrudig
 * @version 1.0
 */
public class RetrieveDataException extends Exception {
    public RetrieveDataException(String message) {
        super(message);
    }
    public RetrieveDataException(){
        super("Fehler beim Einlesen der Datei");
    }
}
