package utility;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Die abstrakte Klasse implementiert das Erstellen von gultigen Datums.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class DateBuilder {
    /**
     * Methode generiert aus den eingegeben Daten ein gueltiges Datum in der Zukunft und liefert dieses in Form
     * eines GregorianCalendar zurück.
     *
     * @return Liefert ein Datum in der Zukunft vom Typ GregorianCalendar zurück.
     */
    public static GregorianCalendar getDeadlineDatum() {
        boolean datumInZukunft = false;
        Calendar cal = new GregorianCalendar();
        GregorianCalendar deadlineDate = new GregorianCalendar();
        do {
            eingabeDatumTag();
            int day = InputChecker.getWertInnerhalbGueltigemBereich(1, 31);
            eingabeDatumMonat();
            int month = InputChecker.getWertInnerhalbGueltigemBereich(1, 12) - 1;
            int year = cal.get(Calendar.YEAR);
            eingabeZeitStunde();
            int hour = InputChecker.getWertInnerhalbGueltigemBereich(0, 23);
            eingabeZeitMinute();
            int minute = InputChecker.getWertInnerhalbGueltigemBereich(0, 59);
            deadlineDate = new GregorianCalendar(year, month, day, hour, minute);
            if (deadlineDate.before(getAktuellesDatum())) {
                fehlermeldungDeadline();
                datumInZukunft = false;
            } else {
                datumInZukunft = true;
            }
        } while (!datumInZukunft);

        return deadlineDate;
    }

    /**
     * Methode generiert ein gueltiges aktuelles Datum und liefert dieses in Form eines GregorianCalendar zurück.
     *
     * @return Liefert das aktuelle Datum vom Typ GregorianCalendar zurück.
     */
    public static GregorianCalendar getAktuellesDatum() {
        GregorianCalendar deadlineDate = new GregorianCalendar();
        deadlineDate.getTime();
        return deadlineDate;
    }

    //Augaben
    public static void fehlermeldungDeadline() {
        System.out.println("Das Eingegebene Datum liegt nicht in der Zukunft!");
    }

    public static void eingabeDatumTag() {
        System.out.println("Bitte geben Sie den gewünschten Tag der Deadline ein.");
    }

    public static void eingabeDatumMonat() {
        System.out.println("Bitte geben Sie den gewünschten Monat der Deadline ein.");
    }

    public static void eingabeZeitStunde() {
        System.out.println("Bitte geben Sie den gewünschte Stunde der Deadline ein.");
    }

    public static void eingabeZeitMinute() {
        System.out.println("Bitte geben Sie den gewünschte Minute der Deadline ein.");
    }

}
