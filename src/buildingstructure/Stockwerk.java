package buildingstructure;

import exceptions.InvalidInputException;

import java.io.Serializable;

/**
 * Die Klasse implementiert ein Stockwerk.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class Stockwerk implements Serializable {
    //Datenfelder
    public static final int ANZAHL_ZEICHEN_BEZEICHNUNG = 16;
    private String bezeichnung;

    /**
     * Konstruktor, welcher beim Erzeugen von Objekten im Fehlerfall Exceptions(InvalidFloorException,
     * InvalidInputException) ausgelöst.
     *
     * @param bezeichnung übergebene Bezeichnung des Raumes vom Typ String.
     * @throws InvalidInputException beim Übergeben einer ungültigen Bezeichnung vom Typ String.
     */
    public Stockwerk(String bezeichnung) throws InvalidInputException {
        this.setBezeichnung(bezeichnung);
    }

    /**
     * Getter-Methode, liefert die Beschreibung des Stockwerkes zurück.
     *
     * @return Liefert die Beschreibung eines Stockwerkes vom Typ String zurück.
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Setter-Methode, über die die Beschreibung eines Stockwerkes gesetzt wird und bei Übergabe eines ungültigen
     * Strings(maximale Anzahl an Zeichen) eine Exception(InvalidInputException) auslöst.
     *
     * @param bezeichnung übergebene Bezeichnung des Stockwerkes vom Typ String.
     * @throws InvalidInputException beim Übergeben einer ungültigen Bezeichnung vom Typ String.
     */
    public void setBezeichnung(String bezeichnung) throws InvalidInputException {
        if (bezeichnung != null && bezeichnung.length() <= ANZAHL_ZEICHEN_BEZEICHNUNG) {
            this.bezeichnung = bezeichnung;
        } else {
            throw new InvalidInputException("Ungueltige Eingabe, max. " + ANZAHL_ZEICHEN_BEZEICHNUNG + " Zeichen");
        }
    }

    /**
     * Die Methode liefert eine Zeichenkette der Datenfelder zurück.
     *
     * @return Liefert einen String der Datenfelder zurück.
     */
    @Override
    public String toString() {
        return String.format("%" + Stockwerk.ANZAHL_ZEICHEN_BEZEICHNUNG +
                "s", this.bezeichnung);
    }
}
