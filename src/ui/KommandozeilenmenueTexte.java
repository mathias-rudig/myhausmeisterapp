package ui;

/**
 * Die Klasse erweitert das Kommandozeilenmenü um Methoden, welche Ausgaben des Menüs darstellen.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class KommandozeilenmenueTexte {

    public static void ausgabeHauptmenueAnzeigen() {
        System.out.println("************************* MENUE **************************");
        System.out.println("1) Alle Störungen anzeigen");
        System.out.println("2) Alle Störungen eines bestimmten Gewerkes anzeigen");
        System.out.println("3) Alle Störungen eines bestimmten Status anzeigen");
        System.out.println("4) Alle Störungen mit einer Deadline ausgeben");
        System.out.println("5) Erstellen");
        System.out.println("6) Ändern");
        System.out.println("7) Löschen");
        System.out.println("8) Speichern");
        System.out.println("x) Beenden");
        ausgabeStrich();
    }

    public static void ausgabeHinzufuegenMenueAnzeige() {
        System.out.println("************************* ANLEGEN ************************");
        System.out.println("1) Störung");
        System.out.println("2) Störung mit Deadline");
        System.out.println("3) Raum");
        System.out.println("4) Stockwerk");
        System.out.println("x) Beenden");
        ausgabeStrich();
    }

    public static void ausgabeAendernStoerungMenueAnzeige() {
        System.out.println("************************* ÄNDERN ************************");
        System.out.println("1) Titel");
        System.out.println("2) Beschreibung");
        System.out.println("3) Gewerk");
        System.out.println("4) Raum");
        System.out.println("5) Status");
        System.out.println("6) Deadline");
        System.out.println("x) Beenden");
        ausgabeStrich();
    }

    public static void ausgabeEntfernenMenueAnzeige() {
        System.out.println("************************* LÖSCHEN ************************");
        System.out.println("1) Störung");
        System.out.println("2) Raum");
        System.out.println("3) Stockwerk");
        System.out.println("x) Beenden");
        ausgabeStrich();
    }

    public static void ausgabeFehlerEingabe() {
        System.out.println("Fehlerhafte Eingabe!");
    }

    public static void ausgabeEingabeaufforderung() {
        System.out.print("> ");
    }

    public static void ausgabeStrich(){
        System.out.println("***********************************************************");
    }
}

